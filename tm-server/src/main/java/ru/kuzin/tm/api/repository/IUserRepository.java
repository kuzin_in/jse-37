package ru.kuzin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findByLogin(@NotNull String login) throws Exception;

    @Nullable
    User findByEmail(@NotNull String email) throws Exception;

    Boolean isLoginExist(@NotNull String login) throws Exception;

    Boolean isEmailExist(@NotNull String email) throws Exception;

    @NotNull
    User update(@NotNull User model) throws Exception;

}