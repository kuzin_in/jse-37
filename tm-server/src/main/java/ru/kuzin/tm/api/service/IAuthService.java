package ru.kuzin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.model.Session;
import ru.kuzin.tm.model.User;

public interface IAuthService {

    @NotNull
    String login(@Nullable String login, @Nullable String password) throws Exception;

    @NotNull
    Session validateToken(@Nullable String token);

    void invalidate(@Nullable Session session) throws Exception;

    @NotNull
    User registry(@Nullable String login, @Nullable String password, @Nullable String email) throws Exception;

}