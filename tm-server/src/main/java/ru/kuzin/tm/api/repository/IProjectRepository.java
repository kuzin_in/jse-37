package ru.kuzin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.model.Project;

import java.sql.ResultSet;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @NotNull
    Project update(@NotNull Project model) throws Exception;

    @NotNull
    Project fetch(@NotNull ResultSet row) throws Exception;

    @NotNull
    Project create(@NotNull String userId, @NotNull String name, @NotNull String description) throws Exception;

    @NotNull
    Project create(@NotNull String userId, @NotNull String name) throws Exception;

}