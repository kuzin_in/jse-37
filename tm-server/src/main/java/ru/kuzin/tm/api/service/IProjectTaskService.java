package ru.kuzin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.model.Task;

public interface IProjectTaskService {

    @NotNull
    Task bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId) throws Exception;

    void removeProjectById(@Nullable String userId, @Nullable String projectId) throws Exception;

    void removeProjectByIndex(@Nullable String userId, @Nullable Integer projectIndex) throws Exception;

    @NotNull
    Task unbindTaskFromProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId) throws Exception;

}