package ru.kuzin.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.api.repository.ISessionRepository;
import ru.kuzin.tm.api.service.IConnectionService;
import ru.kuzin.tm.api.service.ISessionService;
import ru.kuzin.tm.model.Session;

import java.sql.Connection;

public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull ISessionRepository repository, @NotNull IConnectionService connectionService) {
        super(repository, connectionService);
    }

    @Override
    public void setRepositoryConnection(Connection connection) {
        repository.setRepositoryConnection(connection);
    }

}