package ru.kuzin.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
public abstract class AbstractModel implements Serializable {

    @Nullable
    private String id = UUID.randomUUID().toString();

}