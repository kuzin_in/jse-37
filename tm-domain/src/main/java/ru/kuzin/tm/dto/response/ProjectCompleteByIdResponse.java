package ru.kuzin.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.model.Project;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectCompleteByIdResponse extends AbstractProjectResponse {

    public ProjectCompleteByIdResponse(@Nullable final Project project) {
        super(project);
    }

}