package ru.kuzin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.api.service.IPropertyService;
import ru.kuzin.tm.enumerated.Role;
import ru.kuzin.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User create(@NotNull IPropertyService propertyService, @NotNull String login, @NotNull String password);

    @Nullable
    User create(@NotNull IPropertyService propertyService, @NotNull String login, @NotNull String password, @Nullable String email);

    @Nullable
    User create(@NotNull IPropertyService propertyService, @NotNull String login, @NotNull String password, @NotNull Role role);

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User findByEmail(@NotNull String email);

    Boolean isLoginExist(@NotNull String login);

    Boolean isEmailExist(@NotNull String email);

}